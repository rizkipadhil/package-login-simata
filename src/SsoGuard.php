<?php
namespace Rizkipadhil\SimataSso;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;

class SsoGuard implements Guard
{
    protected $provider;

    public function __construct(UserProvider $provider)
    {
        $this->provider = $provider;
    }

    public function hasUser()
    {
        return !is_null($this->user());
    }

    public function id()
    {
        if ($user = $this->user()) {
            return $user->getAuthIdentifier();
        }
        return null;
    }

    public function validate(array $credentials = [])
    {
        $user = $this->provider->retrieveByCredentials($credentials);
        if ($user && $this->provider->validateCredentials($user, $credentials)) {
            // Menyimpan pengguna ke dalam sesi atau melakukan tindakan lain untuk menandai pengguna sebagai terautentikasi
            session(['auth_user' => $user->getAuthIdentifier()]);
            return true;
        }
        return false;
    }

    // Implementasi method-method dari interface Guard...
    public function check()
    {
        return !is_null($this->user());
    }

    public function guest()
    {
        return !$this->check();
    }

    public function user()
    {
        // Cek jika kita sudah memiliki user yang tersimpan di sesi
        if (session()->has('auth_user')) {
            $userId = session('auth_user');
            return $this->provider->retrieveById($userId);
        }

        return null;
    }
    /**
     * Set the current user.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $user
     * @return void
     */
    public function setUser(Authenticatable $user)
    {
        $this->user = $user;
    }
}
