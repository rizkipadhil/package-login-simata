<?php

namespace Rizkipadhil\SimataSso;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Models\User;

class SsoUserProvider implements UserProvider
{
  protected $ssoClient;

  public function __construct()
  {
    $this->ssoClient = new SSOClient(); // Inisialisasi SSOClient
  }
  public function retrieveById($identifier)
  {
    $token = session('sso_token');
    if (!$token) {
      return null;
    }
    // Implementasi retrieve user by ID
    $user = $this->ssoClient->getUserByToken($token);

    if ($user['id'] == $identifier) {
      // Asumsikan respons memiliki struktur yang memungkinkan kamu membuat instance User
      return new User($user);
    }

    return null;
  }

  public function retrieveByToken($identifier, $token)
  {
    // Implementasi retrieve user by token
  }

  public function updateRememberToken(Authenticatable $user, $token)
  {
    // Update remember token jika diperlukan
  }

  public function retrieveByCredentials(array $credentials)
  {
    // Implementasi retrieve user by credentials
  }

  public function validateCredentials(Authenticatable $user, array $credentials)
  {
    // Dalam konteks SSO, validasi bisa jadi hanya memeriksa bahwa user telah di-authenticate
    return $user !== null;
  }
}
