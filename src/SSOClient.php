<?php
// src/SSOClient.php

namespace Rizkipadhil\SimataSso;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class SSOClient
{
    protected $endpoint;
    protected $sessionTokenKey = 'sso_token';
    protected $sessionUserKey = 'user';

    public function __construct()
    {
        $this->endpoint = config('simata-sso.api_endpoint', 'https://simatasehat.rsmprovkaltim.com');
    }

    /**
     * Authenticate user and store session
     */
    public function authenticate(Request $request)
    {
        $response = Http::post($this->endpoint . '/api/login', [
            'username' => $request->username,
            'password' => $request->password,
        ]);

        if ($response->successful()) {
            $result = $response->json();
            $data = $result['data'];

            // Store token and user data in session
            $this->storeSession($data);

            return $data;
        }

        return false;
    }

    /**
     * Get user data by token and update session
     */
    public function getUserByToken($token)
    {
        $response = Http::withToken($token)
            ->get($this->endpoint . '/api/user');

        if ($response->successful()) {
            $userData = $response->json();
            
            // Update user data in session
            session([$this->sessionUserKey => json_encode($userData)]);
            
            return $userData;
        }

        return false;
    }

    /**
     * Store data in session
     */
    protected function storeSession($data)
    {
        if (isset($data['token'])) {
            session([$this->sessionTokenKey => $data['token']]);
        }
        
        if (isset($data['user'])) {
            session([$this->sessionUserKey => json_encode($data['user'])]);
        }
        
        // Regenerate session for security
        session()->regenerate();
    }

    /**
     * Get current token from session
     */
    public function getSessionToken()
    {
        return session($this->sessionTokenKey);
    }

    /**
     * Get current user from session
     */
    public function getSessionUser()
    {
        $userData = session($this->sessionUserKey);
        return $userData ? json_decode($userData, true) : null;
    }

    /**
     * Check if session is valid
     */
    public function hasValidSession()
    {
        return session()->has($this->sessionTokenKey) && 
               session()->has($this->sessionUserKey);
    }

    /**
     * Clear session data
     */
    public function clearSession()
    {
        session()->forget([$this->sessionTokenKey, $this->sessionUserKey]);
        session()->invalidate();
        session()->regenerateToken();
    }

    /**
     * Logout user and clear session
     */
    public function logout()
    {
        $token = $this->getSessionToken();
        
        if ($token) {
            // Optional: call logout endpoint if needed
            // Http::withToken($token)->post($this->endpoint . '/api/logout');
            
            $this->clearSession();
            return true;
        }
        
        return false;
    }
}