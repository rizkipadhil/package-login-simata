<?php
namespace Rizkipadhil\SimataSso;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Exception;

class AuthController extends Controller
{
    protected $ssoClient;

    public function __construct(SSOClient $ssoClient)
    {
        $this->ssoClient = $ssoClient;
        // Memastikan middleware session berjalan
        $this->middleware('simata-sso')->except(['login']);
    }

    /**
     * Handle SSO login
     * 
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        try {
            $authData = $this->ssoClient->authenticate($request);

            if (!$authData || !isset($authData['token'])) {
                return response()->json(['error' => 'Invalid credentials'], 401);
            }

            // Gunakan helper function untuk set session
            $this->setUserSession($authData);

            // Menggunakan token untuk mendapatkan data user
            $userData = $this->ssoClient->getUserByToken($authData['token']);
            if (!$userData) {
                $this->clearUserSession();
                return response()->json(['error' => 'Failed to retrieve user data'], 401);
            }

            // Logika untuk menciptakan atau mendapatkan user dan login
            $user = $this->findOrCreateUser($userData);
            Auth::login($user);

            return response()->json([
                'message' => 'Login successful',
                'user' => $user,
                'token' => $authData['token']
            ]);

        } catch (Exception $e) {
            $this->clearUserSession();
            return response()->json(['error' => 'Server error', 'message' => $e->getMessage()], 500);
        }
    }

    public function getUser()
    {
        $user = json_decode(session(config('simata-sso.session.user_key')), true);
        return response()->json(['user' => $user]);
    }

    /**
     * Handle SSO logout
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            // Logout dari SSO server jika perlu
            $token = session(config('simata-sso.session.token_key'));
            if ($token) {
                $this->ssoClient->logout($token);
            }

            // Clear local session dan logout
            $this->clearUserSession();
            Auth::logout();

            return response()->json(['message' => 'Logout successful']);
        } catch (Exception $e) {
            return response()->json(['error' => 'Logout failed', 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * Set session data after successful authentication
     * 
     * @param array $authData
     */
    protected function setUserSession(array $authData)
    {
        $sessionData = [
            config('simata-sso.session.token_key') => $authData['token'],
            config('simata-sso.session.user_key') => json_encode($authData['user'])
        ];

        foreach ($sessionData as $key => $value) {
            session([$key => $value]);
        }

        // Regenerate session ID for security
        session()->regenerate();
    }

    /**
     * Clear user session data
     */
    protected function clearUserSession()
    {
        session()->forget([
            config('simata-sso.session.token_key'),
            config('simata-sso.session.user_key')
        ]);
        
        session()->invalidate();
        session()->regenerateToken();
    }

    /**
     * Check if user has valid session
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkSession()
    {
        $token = session(config('simata-sso.session.token_key'));
        $user = json_decode(session(config('simata-sso.session.user_key')), true);

        if (!$token || !$user) {
            return response()->json(['authenticated' => false], 401);
        }

        return response()->json([
            'authenticated' => true,
            'user' => $user
        ]);
    }
}