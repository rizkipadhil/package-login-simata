<?php
namespace Rizkipadhil\SimataSso;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Rizkipadhil\SimataSso\SSOClient;
use Rizkipadhil\SimataSso\SsoGuard;
use Rizkipadhil\SimataSso\SsoUserProvider;

class SimataSsoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        // Publish config
        $this->publishes([
            __DIR__ . '/../config/simata-sso.php' => config_path('simata-sso.php'),
        ], 'config');

        // Load routes
        $this->loadRoutesFrom(__DIR__ . '/routes.php');

        // Register SSO auth provider
        Auth::provider('sso', function ($app, array $config) {
            return new SsoUserProvider($app['hash'], $config['model']);
        });

        // Ensure session middleware is registered
        $this->app['router']->middlewareGroup('simata-sso', [
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        ]);

        // Merge config
        $this->mergeConfigFrom(
            __DIR__ . '/../config/simata-sso.php', 'simata-sso'
        );
    }

    /**
     * Register any application services.
     */
    public function register()
    {
        // Register SSO Client
        $this->app->singleton(SSOClient::class, function ($app) {
            return new SSOClient();
        });

        // Register SSO Guard
        $this->app['auth']->extend('sso-guard', function ($app, $name, array $config) {
            $provider = $app['auth']->createUserProvider($config['provider'] ?? null);
            $guard = new SsoGuard($provider);
            
            // Ensure guard has access to session
            if ($this->app->bound('session.store')) {
                $guard->setSession($this->app['session.store']);
            }
            
            return $guard;
        });

        // Register session store if not already bound
        if (!$this->app->bound('session.store')) {
            $this->app->singleton('session.store', function ($app) {
                return $app['session']->driver();
            });
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [SSOClient::class, 'auth.provider.sso'];
    }
}