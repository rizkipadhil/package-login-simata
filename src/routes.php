<?php
// src/routes.php

use Illuminate\Support\Facades\Route;
use Rizkipadhil\SimataSso\AuthController;

Route::prefix('simata-sso')->group(function () {
    // Public routes
    Route::post('/login', [AuthController::class, 'login']);
    
    // Protected routes (memerlukan session)
    Route::middleware('simata-sso')->group(function () {
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::get('/check-session', [AuthController::class, 'checkSession']);
        Route::get('/me', [AuthController::class, 'getUser']);
    });
});