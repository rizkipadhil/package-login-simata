<?php
// config/simata-sso.php

return [
    'api_endpoint' => env('SIMATA_SSO_ENDPOINT', 'https://localhost:8000'),
    
    // Tambahkan konfigurasi session
    'session' => [
        'token_key' => 'sso_token',
        'user_key' => 'user',
    ],
    
    // Tambahkan konfigurasi cookie jika diperlukan
    'cookie' => [
        'lifetime' => 120,
        'secure' => true,
        'same_site' => 'lax',
    ]
];