# Cara Setup
Tambahkan di require "rsmata/simata-sso": "dev-main"

"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/rizkipadhil/package-login-simata.git"
    }
],

composer update

## 1. Setting Config/auth.php
`
'guards' => [
    'web' => [
        'driver' => 'sso-guard', // Gunakan custom guard
        'provider' => 'users',
    ],
],

'providers' => [
    'users' => [
        'driver' => 'sso',
        // Lainnya...
    ],
],

## 2. Setup ENV
SIMATA_SSO_ENDPOINT=https://simatasehat.rsmprovkaltim.com
